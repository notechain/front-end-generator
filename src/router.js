import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './components/Dashboard.vue'
import Home from './components/Home.vue'
import Reader from './components/Reader.vue'
import About from './components/About.vue'
import Note from './components/Note.vue'
import NoWallet from './components/NoWallet.vue'
import NoMainnet from './components/NoMainnet.vue'
import Login from './components/Login.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/read',
      name: 'read',
      component: Reader,
    },
    {
      path: '/write',
      name: 'dashboard',
      component: Dashboard,
      children: [{
          path: ':note_id',
          name: 'write',
          component: Note
      }]
    },
    {
          path: '/read/:note_id',
          name: 'read_note',
          component: Reader
    },
    {
        path: '/about',
      name: 'about',
      component: About
    },
    {
        path: '/login',
      name: 'Login',
      component: Login
    },
    {
        path: '/heyheyheeeyy',
      name: 'NoWallet',
      component: NoWallet
    },
    {
        path: '/wassawassa',
      name: 'NoMainnet',
      component: NoMainnet
    }
  ]
})

