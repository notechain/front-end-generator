import Vue from 'vue'
import App from './App.vue'

import router from './router'

import VeeValidate from 'vee-validate'

Vue.use(VeeValidate);

import PrettyInput from 'pretty-checkbox-vue/input'
import PrettyCheck from 'pretty-checkbox-vue/check'
import PrettyRadio from 'pretty-checkbox-vue/radio'

Vue.component('p-input', PrettyInput)
Vue.component('p-check', PrettyCheck)
Vue.component('p-radio', PrettyRadio)

Vue.config.productionTip = false

import Tooltip from 'vue-directive-tooltip';
import 'vue-directive-tooltip/css/index.css';
Vue.use(Tooltip);

import Notifications from 'vue-notification'
 
Vue.use(Notifications)

import Loader from './components/Loader'
 
Vue.use(Loader)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
