import Web3 from 'web3'
import { VALID_NETID, CONTRACT_ABI, CONTRACT_ADDRESS, ALTERNATIVE_PROVIDER } from './js/contract_conf.js'
import { sha256 } from './js/sha256.js'
import { encrypt, decrypt } from './js/encryption.js'

const our_message = "Log me in to Notechain."; // never change this ever
const maxGasBudget = 4000000;  // what normally used in remix
const minGasBudget = 150000;  // what normally used in remix
var gasBudget = maxGasBudget; 

var NoteChain = false;

// in wei, the default price for gas
var gasPrice = Math.pow(10,9); 

// in wei, this value will be updated
// 0.0002 ether the default fee
var noteChainFee = 200000000000000;  

var getMyNotesLimit = 10;
var userAccount = false;
var _web3 = false;
var current_network;

// use this to store key
// to make it as private as possible
function MyKey() {
  let k = false; 
  let s = "NoteChain"; 
  this.isKeySet = function () {
    return k != false;
  };
  this.resetKey = function () {
    k = false;
  };
  this.setKey = function (_k) {
    k = sha256(_k);
  };
  this.hashWithKey = function (_w) {
    return sha256(k+_w+s);
  };
}

export function setGasBudget(budget) {
    let adjustedBudget = Math.floor(budget*1.4);

    if (adjustedBudget>maxGasBudget) {
        gasBudget = maxGasBudget;
    } else if (adjustedBudget<minGasBudget) {
        gasBudget = minGasBudget;
    } else {
        gasBudget = adjustedBudget;
    }
}

export function getGasPrice() {
    return gasPrice;
}

export function showNotification(t, msg) {
  t.$notify({
    type: 'normal',
    text: msg
  });
}

export function showSuccess(t, msg) {
  t.$notify({
    type: 'success',
    text: msg
  });
}

export function showWarning(t, msg) {
  t.$notify({
    type: 'warn',
    text: msg
  });
}

export function showError(t, err) {
  //console.log(err);
  t.$notify({
    type: 'error',
    text: String(err).slice(0,200) + ".."
  });
}

export function showLoading(t, msg) {
  t.$Loader(msg);
}
export function hideLoading(t) {
  t.$Loader("");
}

function handleTxConfirmation(t, note, receipt) {
    //console.log('handleTxReceipt');
    //console.log(receipt);
    note.isSynced = true;
    note.syncLocal();
    showSuccess(t, "<b>" + note.title + "</b> has been syncronized with BlockChain!")
}

function handleTx(t, note, hash) {
    //console.log('handleTx');

    hideLoading(t);
    //console.log(note);
    note.isSynced = false;
    note.TXID = hash;
    showNotification(t, "<b>" + note.title + "</b> is syncronizing...");
}

function handlePromiseTXEvents(t, tx, note){
    tx.note = note;
    tx.on('transactionHash', function(hash){
        handleTx(t, this.note, hash);
    })
    .on('confirmation', function(confirmationNumber, receipt){
        if (!this.note.isSynced) {
            if (receipt['status'] == '0x1') {
                //console.log(receipt);
                handleTxConfirmation(t, this.note, receipt);
            } else if (receipt['status'] == '0x0') {
                showError(t, "Syncronization is failed! Probably set a higher gas limit or your account has no funds.");
            }
            hideLoading(t);
            this.note.isSynced = true;
        }
    })
    .on('error', (err) => {
        hideLoading(t);
        showError(t, err);
        tx.note.isSynced = true;
    });
}


let myKey = new MyKey();

export function hasLoggedIn(){
  return userAccount != false;
}

export function getNoteChainFee() {
    return noteChainFee;
}

// for every note, 
// myKey.hashWithKey(publickey);

export function init(t, login) {
    showLoading(t, "Initializing...");

    if (!_web3) {
        try {

            setInterval(function() {
                web3.version.getNetwork((err, netId) => {
                    if (current_network != netId) {
                        current_network = netId;
                        if (current_network != VALID_NETID) {
                            hideLoading(t);
                            t.$router.push({ name: 'NoMainnet'}); 
                            return false;
                        } else {
                            hideLoading(t);
                            t.$router.push({ name: 'dashboard'}); 
                        }
                    }
                });
            }, 250);

            web3.version.getNetwork((err, netId) => {
                current_network = netId;
                if (current_network != VALID_NETID) {
                    hideLoading(t);
                    t.$router.push({ name: 'NoMainnet'}); 
                    return false;
                }
            });
            _web3 = new Web3(web3.currentProvider);
        } catch {
            _web3 = new Web3(new Web3.providers.HttpProvider(ALTERNATIVE_PROVIDER));
            current_network = VALID_NETID;
        }

        //console.log("Initialized");
        //console.log(_web3);
        _web3.eth.getGasPrice().then( res => {
            gasPrice = parseInt(res);
            //console.log("gas price");
            //console.log(res);
            //console.log(typeof res);
        });
        NoteChain = new _web3.eth.Contract(CONTRACT_ABI, CONTRACT_ADDRESS);
        //console.log(NoteChain);

    } else if (current_network != VALID_NETID) {
        hideLoading(t);
        t.$router.push({ name: 'NoMainnet'}); 
        return false;
    }

    if (login) {
        _web3.eth.getAccounts(function(error, accounts) {
            if (!setUserAccount(t, accounts)) {
                return false;
            }
        });
        setInterval(function() {
            _web3.eth.getAccounts(function(error, accounts) {
                if (t.$router.history.current.name == "write")
                    setUserAccount(t, accounts);
            });
        }, 250);
    }

    hideLoading(t);
    return true;

}


function setUserAccount(t, accounts) {
  if (!accounts.length) {
    showWarning(t, "Wallet is not detected..")
    hideLoading(t);
    t.$router.push({ name: 'NoWallet'}); 
    return false;
  }

  if (userAccount != accounts[0]) {
    userAccount = accounts[0];
    myKey.resetKey();
  }

  if (!myKey.isKeySet()) {
    hideLoading(t);
    showWarning(t, "Please login before continue..");
    //userAccount = false;
    t.$router.push({ name: 'Login'}); 
  }
  return true;

}

export function isKeySet(){
  return myKey.isKeySet();
}

export function getKey(t){
  showLoading(t, "A Metamask dialog has been popped up. The dialog is asking you to sign a message, please sign it to log in.");

  try {
    _web3.eth.personal.sign(our_message, 
        userAccount)
      .then( res => {
        myKey.setKey(res);
        t.$router.push({ name: 'dashboard'}); 
        showNotification(t, 'Hello ' + String(userAccount).slice(0,12) + "...");
      }).catch( err => {
        showError(t, err);
      }).then( () => {
        hideLoading(t);
      });
  } catch (err) {
    hideLoading(t);
    showError(t, err);
    t.$router.push({ name: 'home'}); 
  }
}

// views 

function getMyNote(_noteId) {
  return NoteChain.methods.getMyNote(_noteId).call({from: userAccount});
}

function getMyNotes(_startFrom, _limit) {
  return NoteChain.methods.getMyNotes(_startFrom, _limit).call({from: userAccount});
}

function getNotesCount() {
  return NoteChain.methods.getNotesCount().call();
}

function doGetNoteChainFee() {
  return NoteChain.methods.noteChainFee().call();
}

function publicGetNote (_noteId, _publicKey) {
  return NoteChain.methods.publicGetNote(_noteId, _publicKey).call();
}

// payable 

function createNote(_metadata, _publicKey, _title, _content) {
  return NoteChain.methods.createNote(_metadata, _publicKey, _title, _content).send(
      { from: userAccount, gasPrice: gasPrice, gas: gasBudget, value: noteChainFee });
}

function deleteNote(_noteId) {
  return NoteChain.methods.deleteNote(_noteId).send(
      { from: userAccount, gasPrice: gasPrice, gas: gasBudget, value: noteChainFee });
}

function updateNote(_noteId, _metadata, _title, _content) {
  return NoteChain.methods.updateNote(_noteId, _metadata, _title, _content).send(
      { from: userAccount, gasPrice: gasPrice, gas: gasBudget, value: noteChainFee });
}

function updateNoteMetadata(_noteId, _metadata) {
  return NoteChain.methods.updateNoteMetadata(_noteId, _metadata).send(
      { from: userAccount, gasPrice: gasPrice, gas: gasBudget, value: noteChainFee });
}

function updateNoteContent(_noteId, _content) {
  return NoteChain.methods.updateNoteContent(_noteId, _content).send(
      { from: userAccount, gasPrice: gasPrice, gas: gasBudget, value: noteChainFee });
}

function updateNoteTitle(_noteId, _title) {
  return NoteChain.methods.updateNoteTitle(_noteId, _title).send(
      { from: userAccount, gasPrice: gasPrice, gas: gasBudget, value: noteChainFee });
}

function updateNoteButContent(_noteId, _metadata, _title) {
  return NoteChain.methods.updateNoteButContent(_noteId, _metadata, _title).send(
      { from: userAccount, gasPrice: gasPrice, gas: gasBudget, value: noteChainFee });
}

//ownerGetNote('1').then( result => {
//console.log(result);
//});
//
const isPrivatePos = 1;
const isEncryptedPos = 2;
const encryptionLevelStartPos = 3;
const encryptionLevelLen = 5;

function getValueBitFromLen(d,s,l) {
  return (d >> s) % Math.pow(2,l);
}

function isPosBitOne(d,p) {
  return (d % Math.pow(2,p+1)) >= Math.pow(2,p);
}

function toAscii(a){
  return _web3.utils.toAscii(a);
}

function toBytes(a){
  return _web3.utils.asciiToHex(a);
}

export class Note {

  constructor(intNoteId, metadata, publicKey, title, content
      , isFetched, isNew) {

    try {
      title = toAscii(title).replace(/\u0000/g,'');
      content = toAscii(content);
    } catch {

    }

    this.password = "";
    this.password2 = "";

    this._publicKey = publicKey;
    this._intNoteId = intNoteId;

    this.noteId = publicKey.slice( publicKey.indexOf('x')+1) + intNoteId.toString(16);
    this.title = title;
    this.isPrivate = isPosBitOne(metadata, isPrivatePos);
    this.isEncrypted = isPosBitOne(metadata, isEncryptedPos);
    this.encryptionLevel = getValueBitFromLen(metadata,
        encryptionLevelStartPos, encryptionLevelLen);

    this._encryptedContent = content;
    this.isEncrypting = false;

    this.content = "";

    this.isDecrypted = true;

    // backup for comparison with blockchain
    this._noteId = this.noteId;
    this._metadata = metadata;
    this._title = this.title;
    this._content = this.content;
    this._isPrivate = this.isPrivate;
    this._isEncrypted = this.isEncrypted;
    this._encryptionLevel = this.encryptionLevel;

    // if all the data has been fetched for this
    this._isFetched = isFetched;

    // if the note is still new
    this._isNew = isNew;

    this.isSynced = true;
    this.TXID = "";

  }

  resetNote() {
    this.title = this._title;
    this.content = this._content;
    this.isPrivate = this._isPrivate;
    this.isEncrypted = this._isEncrypted;
    this.encryptionLevel = this._encryptionLevel;
  }

  syncLocal() {
    this._title = this.title;
    this._content = this.content;
    this._isPrivate = this.isPrivate;
    this._isEncrypted = this.isEncrypted;
    this._encryptionLevel = this.encryptionLevel;
  }

  updateTitle(t) {
    let tx = updateNoteTitle( 
        this._intNoteId,
        toBytes(this.title)
        );
    handlePromiseTXEvents(t, tx, this);
    return true;
  }

  updateMetadata(t, metadata) {
    let tx = updateNoteMetadata( 
        this._intNoteId,
        metadata
        );
    handlePromiseTXEvents(t, tx, this);
    return true;
  }

  updateButContent(t, metadata) {
    let tx = updateNoteButContent( 
        this._intNoteId,
        metadata,
        toBytes(this.title)
        );
    handlePromiseTXEvents(t, tx, this);
    return true;
  }

  updateContent(t) {
    let tx = updateNoteContent( 
        this._intNoteId,
        toBytes(this._encryptedContent)
        );
    handlePromiseTXEvents(t, tx, this);
    return true;
  }

  update(t, metadata) {
    let tx = updateNote( 
        this._intNoteId,
        metadata,
        toBytes(this.title),
        toBytes(this._encryptedContent)
        );
    handlePromiseTXEvents(t, tx, this);
    return true;
  }

  submitNewNote(t, newMetadata) {
    //console.log('submitNewNote');
    let tx = createNote( 
        newMetadata, 
        this._publicKey,
        toBytes(this.title), 
        toBytes(this._encryptedContent),
        );
    tx.note = this;
    tx.on('transactionHash', function(hash){
        handleTx(t, this.note, hash);
    })
    .on('error', function(err){
        this.note.isSynced = true;
        hideLoading(t);
        showError(t, err);
    });

    tx.then( receipt => {
            //console.log(receipt);
            let intId;
            try {
                intId = receipt['events']['NoteCreated']['returnValues']['id'];
            } catch (err) {
                showError(t, "Syncronization is failed! Probably set a higher gas limit or your account has no funds.");
                hideLoading(t);
                t.ActiveNote.isSynced = true;
                return false;
            }
            let noteId = this._publicKey.slice(this._publicKey.indexOf('x')+1) + intId.toString(16);

            this._intNoteId = intId;
            this.noteId = noteId;
            this._isNew = false;
            this._isFetched = true;
            this.isSynced = true;
            this.syncLocal();

            t.$options._parentVnode.componentInstance.$options.parent.notes.push(this);
            t.$options._parentVnode.componentInstance.$options.parent.ActiveNote = this;
            t.$options._parentVnode.componentInstance.$options.parent.totalNotesCount++;
            showSuccess(t, "<b>" + this.title + "</b> has been added to your notes...");
        }
      ).catch( err => {
        hideLoading(t);
        //showError(t, err);
      });
    return true;
  }

  submitNote(t) {
    //console.log(t);
    let shouldChangeContent = false;
    // if: 1. changing the content from private to public and vice versa
    // or
    // 2. if we change the encryption level when it's not private
    // we have to change the content also
    if (this.isPrivate != this._isPrivate) {
      shouldChangeContent = true;
    } else if (!this.isPrivate && 
        (this.isEncrypted != this._isEncrypted ||
        this.encryptionLevel != this._encryptionLevel)) {
      // we only care about isEncrypted and encryptionLevel
      // if it's public (not private)
      shouldChangeContent = true;
    }

    if (this._content != this.content || shouldChangeContent || this._isNew) {
        this.isEncrypting = true;
        this.encryptContent(t);
    }
    this.isEncryptingDone(t, shouldChangeContent);

  }

  isEncryptingDone(t, shouldChangeContent) {
      //console.log('isEncryptingDone');
      var theNote = this;
      if(this.isEncrypting) {
          setTimeout( function() {
              theNote.isEncryptingDone(t, shouldChangeContent);
                      }, 250);
      } else {
          this.doSubmitNote(t, shouldChangeContent);
      }
  }

  doSubmitNote(t, shouldChangeContent) {

    let newMetadata = 
      this.isPrivate << isPrivatePos |
      this.isEncrypted << isEncryptedPos |
      this.encryptionLevel << encryptionLevelStartPos;

    let loadingtext = "A Metamask dialog has been popped up. The dialog is asking you to submit a transaction with your wallet. Submit it if you want to store the note.";
    showLoading(t, loadingtext);
    if (this._isNew)  {
      //console.log('submitNewNote');
      return this.submitNewNote(t, newMetadata);
    }

    if (this._content != this.content || shouldChangeContent) {

      if (this._metadata == newMetadata &&
          this._title == this.title) {
        //console.log('updateContent');
        return this.updateContent(t);
      } else {
        //console.log('update');
        return this.update(t, newMetadata);
      }

    }

    if (this._metadata != newMetadata &&
        this._title != this.title) {
      //console.log('updateButContent');
      return this.updateButContent(t, newMetadata);
    }

    if (this._metadata != newMetadata) {
      //console.log('updateMetadata');
      return this.updateMetadata(t, newMetadata);
    }

    if (this._title != this.title) {
      //console.log('updateTitle');
      return this.updateTitle(t);
    }

    showError(t, "Undetected operation, please report to admin");
    hideLoading(t);

  }

  deleteThis(t) {
    showLoading(t, "A Metamask dialog has been popped up. The dialog is asking you to submit a transaction with your wallet. Submit it if you want to delete the note.");
    let tx = deleteNote(this._intNoteId);

    tx.note = this;
    tx.on('transactionHash', function(hash){
        handleTx(t, this.note, hash);
    })
    .on('error', function(err){
        this.note.isSynced = true;
        hideLoading(t);
        showError(t, err);
    });

    tx.then( receipt => {
        //console.log('deleteThis');
        //console.log(res);
        //console.log(t);
        //console.log(t.$options._parentVnode.componentInstance.$options.parent.notes); 
        //console.log(t.$options._parentVnode.componentInstance.$options.parent.notes.length); 
        if (receipt['status'] == '0x0') {
            showError(t, "Syncronization is failed! Probably set a higher gas limit or your account has no funds.");
            tx.note.isSynced = true;
            hideLoading(t);
            return;
        }
        let delNotePos = -1;
        for (let i=0; i < t.$options._parentVnode.componentInstance.$options.parent.notes.length; i++ ) {
            if (t.$options._parentVnode.componentInstance.$options.parent.notes[i]._intNoteId == tx.note._intNoteId) {
                delNotePos = i;
            }
        }
        //console.log(delNotePos);
        tx.note._intNoteId = 0;
        if (delNotePos >= 0) {
            //console.log('deleting');
            showNotification(t, "<b>" + tx.note.title + "</b> has been deleted...");
            tx.note.isSynced = true;
            t.$router.push({ name: 'dashboard'}); 
            t.$options._parentVnode.componentInstance.$options.parent.note_id = 0;
            t.$options._parentVnode.componentInstance.$options.parent.notes.splice(delNotePos,1);
        }
    })
    .catch( err => {
        showError(t, err);
        this.note.isSynced = true;
        hideLoading(t);
        showError(t, "Syncronization is failed! Probably set a higher gas limit or your account has no funds.");
    })
    .then( () => {
      hideLoading(t);
    });
    return true;
  }

  decryptContent(t) {
      let key;
      let level;
      if (this.isPrivate) {
          key = myKey.hashWithKey(this._publicKey);
          level = -1;
      } else if (!this.isEncrypted) {
          level = -1;
          key = "";
      } else {
        key = this.password;
        level = this.encryptionLevel;
      }

      decrypt(this._encryptedContent, key, 
              this._publicKey, level, this, t);
      this.password = "";
      this.password2 = "";
  }

  encryptContent(t) {
      let key;
      let level;
      if (this.isPrivate) {
          key = myKey.hashWithKey(this._publicKey);
          level = -1;
      } else if (!this.isEncrypted) {
          level = -1;
          key = "";
      } else {
        key = this.password;
        level = this.encryptionLevel;
      } 

      encrypt(this.content, key, 
              this._publicKey, level, this, t);
  }

}

function generateRandomKey() {
  let x = _web3.utils.randomHex(2).slice(2);
  if (x.length != 4)
    return generateRandomKey()
  else
    return "0x"+x;
}

export function getInitializedNote(notesCount) {
  return new Note(notesCount, 2, generateRandomKey(), "", "", false, true);
}

export function getInvalidNote() {
  return new Note(0, 2, "", "", "", false, false);
}

export function loadMyNote(t, noteId) {
  t.note_id = noteId;
  let intId = parseInt(String(noteId).slice(4), 16);

  showLoading(t,"Fetching note from blockchain..");

  t.showError = false;

  //console.log('loadMyNote');

  getMyNote(intId).then( res => {

    t.ActiveNote._metadata = res[0];
    t.ActiveNote.title = toAscii(res[1]).replace(/\u0000/g,'');

    t.ActiveNote.isPrivate = isPosBitOne(res[0], isPrivatePos);
    t.ActiveNote.isEncrypted = isPosBitOne(res[0], isEncryptedPos);
    t.ActiveNote.encryptionLevel = getValueBitFromLen(res[0],
        encryptionLevelStartPos, encryptionLevelLen);

    t.ActiveNote._encryptedContent = toAscii(res[2]);

    t.ActiveNote._isFetched = true;
    t.ActiveNote._isNew = false;

    t.ActiveNote._title = t.ActiveNote.title;
    t.ActiveNote._isPrivate = t.ActiveNote.isPrivate;
    t.ActiveNote._isEncrypted = t.ActiveNote.isEncrypted;
    t.ActiveNote._encryptionLevel = t.ActiveNote.encryptionLevel;

    if (t.ActiveNote.isEncrypted) {
        // we should ask password
        t.ActiveNote.isDecrypted = false;
    } else {
        // just decrypt directly without password
        t.ActiveNote.decryptContent(t);
        t.ActiveNote.isDecrypted = true;
    }

  }).catch(  err => {
    showError(t, err);
    t.ActiveNote = getInvalidNote();
  }).then( () => {
    hideLoading(t);
    //t.$router.push({ name: 'write', 
      //params: { note_id : noteId }}); 
  });

}

export function loadNote(t, noteId) {
  t.note_id = noteId;
  let p = "0x" + String(noteId).slice(0,4);
  let k = String(noteId).slice(4);

  showLoading(t,"Fetching note from blockchain..");

  t.showError = false;

  try {
      publicGetNote(k, p).then( res => {
          t.displayedNoteId = noteId;

          t.ActiveNote = new Note(k, 
                  res[0], p, toAscii(res[1]), toAscii(res[2]), true, false);

          // private would failed just check if it's encrypted
          if (t.ActiveNote.isEncrypted) {
              // we should ask password
            t.ActiveNote.isDecrypted = false;
          } else {
            // just decrypt directly without password
            t.ActiveNote.decryptContent(t);
            t.ActiveNote.isDecrypted = true;
          }
      }).catch(  err => {
          t.showError = true;
          showError(t, err);
      }).then( () => {
          hideLoading(t);
          t.$router.push({ name: 'read_note', 
              params: { note_id : noteId }}); 

      });
  } catch (err) {
      showError(t, err);
      hideLoading(t);
      t.$router.push({ name: 'read_note', 
          params: { note_id : noteId }}); 
  }
}

function pushToNotes(notes, obj) {
  let isFound = false;
  //console.log(notes.length);
  for(let i = 0; i < notes.length; i++) {
    //console.log(i);
    if (notes[i].noteId == obj.noteId) {
      isFound = true;
      break;
    }
  }
  if (!isFound) {
    notes.push(obj);
  }
}

function delayPushToNotes(t, res, i, counter) {
  setTimeout(function() {
    pushToNotes(
        t.notes, new Note(
          res[0][i], 
          res[1][i], 
          res[2][i],
          res[3][i], 
          "",
          false, false));
  }, 250*counter);
}

function loadMyNotes(t, startFrom) {

  if (!myKey.isKeySet()) {
      return;
  }
  showLoading(t, "Getting your notes...");
  //console.log('loadMyNotes');
  //console.log(startFrom);

  getMyNotes(startFrom, getMyNotesLimit).then( res => {
      //console.log(res);
      let counter = 0;
      for (let i = 0; i < res[0].length; i++) {
          if (res[1][i] != 1) {
              delayPushToNotes(t, res, i, counter);
              counter++;
          }
      }

      if ( res[0].length == getMyNotesLimit 
            && (startFrom+getMyNotesLimit) < res[4] )
        loadMyNotes(t, startFrom+getMyNotesLimit);
  }).catch( err => {
    //console.log(err);
    showError(t, err);
  }).then( () => {
    hideLoading(t);
  });

}

export function initializeDashboard(t) {

  showLoading(t, "Initializing your dashboard...");

  t.notes = [];
  getNotesCount().then( result => {

    t.totalNotesCount = parseInt(result);
    loadMyNotes(t, 0);
    doGetNoteChainFee().then( res => {
      noteChainFee = parseInt(res);
    })

  }).catch( err => {
    showError(t, err);
  }).then( () => {
    hideLoading(t);
  }
  );
}

