import Loader from './Loader.vue'
import { events } from './events.js'

const theLoader = {
  install(Vue, msg = "") {
    if (this.installed) {
      return
    }

    this.installed = true

    Vue.component('Loader', Loader)

    Vue.prototype.$Loader = (msg) => {
      events.$emit('setMsg', msg)
    }
  }
}

export default theLoader
