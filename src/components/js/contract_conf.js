export var VALID_NETID = "1"; // 1 is for mainnet, 3 for ropsten
export var ALTERNATIVE_PROVIDER = 'https://mainnet.infura.io/1P0DSDbk7z2gRz4jLoag';
//export var ALTERNATIVE_PROVIDER = 'https://api.myetherapi.com/rop';

export var CONTRACT_ADDRESS = '0xD4737858Dd8eb9842CD0696171b781525B56086f';
export var CONTRACT_ABI = 
[
	{
		"constant": true,
		"inputs": [],
		"name": "getBalance",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_noteId",
				"type": "uint64"
			},
			{
				"name": "_metadata",
				"type": "uint16"
			},
			{
				"name": "_title",
				"type": "bytes12"
			},
			{
				"name": "_content",
				"type": "bytes"
			}
		],
		"name": "updateNote",
		"outputs": [],
		"payable": true,
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_noteId",
				"type": "uint64"
			},
			{
				"name": "_title",
				"type": "bytes12"
			}
		],
		"name": "updateNoteTitle",
		"outputs": [],
		"payable": true,
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_noteId",
				"type": "uint64"
			},
			{
				"name": "_publicKey",
				"type": "bytes2"
			}
		],
		"name": "publicGetNote",
		"outputs": [
			{
				"name": "",
				"type": "uint16"
			},
			{
				"name": "",
				"type": "bytes12"
			},
			{
				"name": "",
				"type": "bytes"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_fee",
				"type": "uint256"
			}
		],
		"name": "setFee",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_noteId",
				"type": "uint64"
			},
			{
				"name": "_content",
				"type": "bytes"
			}
		],
		"name": "updateNoteContent",
		"outputs": [],
		"payable": true,
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_metadata",
				"type": "uint16"
			},
			{
				"name": "_publicKey",
				"type": "bytes2"
			},
			{
				"name": "_title",
				"type": "bytes12"
			},
			{
				"name": "_content",
				"type": "bytes"
			}
		],
		"name": "createNote",
		"outputs": [],
		"payable": true,
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "noteChainFee",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "getNotesCount",
		"outputs": [
			{
				"name": "",
				"type": "uint64"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "owner",
		"outputs": [
			{
				"name": "",
				"type": "address"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_noteId",
				"type": "uint64"
			},
			{
				"name": "_metadata",
				"type": "uint16"
			},
			{
				"name": "_title",
				"type": "bytes12"
			}
		],
		"name": "updateNoteButContent",
		"outputs": [],
		"payable": true,
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_noteId",
				"type": "uint64"
			}
		],
		"name": "deleteNote",
		"outputs": [],
		"payable": true,
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_startFrom",
				"type": "uint64"
			},
			{
				"name": "_limit",
				"type": "uint64"
			}
		],
		"name": "getMyNotes",
		"outputs": [
			{
				"name": "",
				"type": "uint64[]"
			},
			{
				"name": "",
				"type": "uint16[]"
			},
			{
				"name": "",
				"type": "bytes2[]"
			},
			{
				"name": "",
				"type": "bytes12[]"
			},
			{
				"name": "",
				"type": "uint64"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_noteId",
				"type": "uint64"
			}
		],
		"name": "getMyNote",
		"outputs": [
			{
				"name": "",
				"type": "uint16"
			},
			{
				"name": "",
				"type": "bytes12"
			},
			{
				"name": "",
				"type": "bytes"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_noteId",
				"type": "uint64"
			},
			{
				"name": "_metadata",
				"type": "uint16"
			}
		],
		"name": "updateNoteMetadata",
		"outputs": [],
		"payable": true,
		"stateMutability": "payable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "newOwner",
				"type": "address"
			}
		],
		"name": "transferOwnership",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_address",
				"type": "address"
			},
			{
				"name": "_amount",
				"type": "uint256"
			}
		],
		"name": "withdraw",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": false,
				"name": "id",
				"type": "uint64"
			},
			{
				"indexed": false,
				"name": "publicKey",
				"type": "bytes2"
			}
		],
		"name": "NoteCreated",
		"type": "event"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"name": "previousOwner",
				"type": "address"
			},
			{
				"indexed": true,
				"name": "newOwner",
				"type": "address"
			}
		],
		"name": "OwnershipTransferred",
		"type": "event"
	}
];
